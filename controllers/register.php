<?php
class Register extends Controller
{
    public function __construct()
    {
        parent::__construct('register');
    }

    public function index()
    {
        $this->views->render('register/index');
    }

}
