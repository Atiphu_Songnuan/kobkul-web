<?php
class Main extends Controller
{
    public function __construct()
    {
        parent::__construct('main');
    }

    public function index()
    {
        $this->views->render('main/index');
    }

}
