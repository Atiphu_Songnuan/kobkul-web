<?php

// Start session for login
session_start();
class Router
{
    public function __construct()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        // print_r($url);
        // echo '<br/>';

        if (empty($url[0])) {
            require_once './controllers/login.php';
            $controller = new Login();
            $controller->index();
            return false;
        }

        $file = './controllers/' . $url[0] . '.php';
        $fileApi = './libs/' . $url[0] . '.php';
        //ตรวจสอบไฟล์ใน folder controllers ว่ามีหรือไม่มี ถ้าไม่มีให้เรียก notfound.php ขึ้นมาแสดง
        if (file_exists($file)) {
            require_once $file;
        } else if (file_exists($fileApi)) {
            require_once $fileApi;
        } else {
            $this->error();
            // require_once 'controllers/notfound.php';
            // $controller = new NotFound();
            // return false;
            // throw new Exception("The File: $file does not exists!");
        }

        $controller = new $url[0];
        $controller->loadModel($url[0]);
        // session_start();
        //ตรวจสอบข้อมูลที่ส่งผ่านมาทาง url ว่ามีการเข้าถึง method ไหน หรือมีการส่งค่าอะไรเข้าไปใน method บ้าง
        // if (isset($url[1])) {
        //     if (isset($url[2])) {
        //         $controller->{$url[1]}($url[2]);
        //         // return false;
        //     } else {
        //         $controller->{$url[1]}();
        //         // return false;
        //     }
        // }

        //Calling methods
        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            } else {
                // echo 'Errrrrrrr';
                $this->error();
            }
        } else {
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    $this->error();
                }

            } else {
                $controller->index();
            }
        }
        
    }

    public function error()
    {
        require_once './controllers/notfound.php';
        $controller = new NotFound();
        $controller->index();
        return false;
    }
}
