<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>เข้าสู่ระบบ</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<!-- Font Kanit -->
	<link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
	<!--===============================================================================================-->
	<script src="https://kit.fontawesome.com/4489a5c865.js" crossorigin="anonymous"></script>
	<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="public/vendor/animate/animate.css"> -->
	<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="public/vendor/css-hamburgers/hamburgers.min.css"> -->
	<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="public/vendor/animsition/css/animsition.min.css"> -->
	<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="public/vendor/select2/select2.min.css"> -->
	<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="public/css/login-util.css">
	<link rel="stylesheet" type="text/css" href="public/css/login-main.css">

	<style>
		::placeholder {
			color: darkred;
		}
	</style>

</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form p-l-55 p-r-55 p-t-178">
					<span class="login100-form-title">
						เข้าสู่ระบบ
					</span>
					
					<i class="fas fa-user-circle"></i>
					<label>ชื่อผู้ใช้งาน</label>
					<div class="wrap-input100 m-b-16">
						<input id="username" class="input100" type="text" name="username" placeholder="example">
						<span class="focus-input100"></span>
					</div>

					<i class="fas fa-lock"></i>
					<label>รหัสผ่าน</label>
					<div class="wrap-input100">
						<input id="password" class="input100" type="password" name="password" placeholder="**********">
						<span class="focus-input100"></span>
					</div>

					<div class="text-right p-t-13 p-b-23">
						<a href="#" class="txt2">
							ลืมรหัสผ่าน
						</a>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="button" onclick="Login()" id="btnlogin">
							ลงชื่อเข้าใช้งาน
						</button>
					</div>

					<div class="flex-col-c p-t-170 p-b-40">
						<span class="txt1 p-b-9">
							Don’t have an account?
						</span>

						<a href="register" class="txt3">
							ลงทะเบียน
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<script src="public/js/main.js"></script>

	<script>
		function Login(){
			console.log("Login");
		}
	</script>
</body>

</html>
